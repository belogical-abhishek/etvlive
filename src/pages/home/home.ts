import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  videoETV1: any = {
    url: 'http://video.vixtream.net/tv/v/ebc1',
    title: 'ETV 1'
  };

  videoETV2: any = {
    url: 'http://video.vixtream.net/tv/v/ebc2',
    title: 'ETV 2'
  };

  videoETV3: any = {
    url: 'http://video.vixtream.net/tv/v/ebc3',
    title: 'ETV 3'
  };

  isVideoETV1Playing: Boolean
  isVideoETV2Playing: Boolean
  isVideoETV3Playing: Boolean

  trustedVideoUrlEVT1: SafeResourceUrl;
  trustedVideoUrlEVT2: SafeResourceUrl;
  trustedVideoUrlEVT3: SafeResourceUrl;
  
  constructor(public navCtrl: NavController, private domSanitizer: DomSanitizer, public admob: AdMobFree) {}

  ionViewWillEnter(): void {
    this.trustedVideoUrlEVT1 = this.domSanitizer.bypassSecurityTrustResourceUrl(this.videoETV1.url);
    this.trustedVideoUrlEVT2 = this.domSanitizer.bypassSecurityTrustResourceUrl(this.videoETV2.url);
    this.trustedVideoUrlEVT3 = this.domSanitizer.bypassSecurityTrustResourceUrl(this.videoETV3.url);
    
    this.isVideoETV1Playing = false;
    this.isVideoETV2Playing = false;
    this.isVideoETV3Playing = false;
    
    this.showBanner();
    this.launchInterstitial();
  }

  launchInterstitial() {
 
    let interstitialConfig: AdMobFreeInterstitialConfig = {
        isTesting: true, // Remove in production
        autoShow: true
        //id: Your Ad Unit ID goes here
    };

    this.admob.interstitial.config(interstitialConfig);

    this.admob.interstitial.prepare().then(() => {
        // success
    });

}

  showBanner() {
    console.log('on load started.');
    let bannerConfig: AdMobFreeBannerConfig = {
      //id:'ca-app-pub-3539139884871368/5703769930',
      isTesting: true, // Remove in production
      autoShow: true,
      //id: Your Ad Unit ID goes here
    };

    this.admob.banner.config(bannerConfig);
    
    this.admob.banner.prepare().then(() => {
        // success
    }).catch(e => console.log(e));
    console.log('on load ended.');
  }

  playVideoETV1() {
    if(this.isVideoETV1Playing) {
      this.isVideoETV1Playing = false;
    } else {
      this.isVideoETV1Playing = true;
    }
  }

  playVideoETV2() {
    if(this.isVideoETV2Playing) {
      this.isVideoETV2Playing = false;
    } else {
      this.isVideoETV2Playing = true;
    }
  }

  playVideoETV3() {
    if(this.isVideoETV3Playing) {
      this.isVideoETV3Playing = false;
    } else {
      this.isVideoETV3Playing = true;
    }
  }
}
